const express = require('express');
const axios = require('axios');
const app = express();
const port = 8000;


let array = [100,101,102,103,201,202,203,204,205,206,300,301,302,303,304,305,306,307,308,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,421,422,423,425,426,428,429,431,451,500,501,502,503,504,505,506,507,511,520,522,524]




app.get('/', (req, res) => {
   res.writeHead(200, { 'Content-type': 'text/html; charset=utf-8' })
   res.write('<h1> Bem vindo </h1> <br> <p>Para acessar o status insira: <strong> /200 </strong> em sua url, deverá estar como o exemplo abaixo: <br> Ex: <strong> localhost:8080/200 </strong> </p> <br> <h1> Caso queira adicionar sleep, insira: </h1> <br> <p> /5000 para 5 segundos. <br> Resultado final: <strong> localhost:8080/200/5000 </strong>')
   res.end()
})


app.get('/:status', async (req, res) => {
    try {
       const status = Number (req.params.status)
        const response = await axios.get(`http://httpstat.us/${status}?sleep=0`)
        const result = response.data
        if(status === 200) {
            res.json({ code: result.code, description: result.description })
        }
        else if(array.includes(status)){
            res.json({ code: `${status}`, message: "Esse valor é um status válido. Disponivel no status HTTP " })
         }
         else{
            throw ({ code: 404, message: "Esse valor não é um status válido." })
            
         }
        
    } catch (err) {
        res.status(404).json({ erro: 'Esse valor não é um status valido ' })
    }
})

app.get('/:status/:tempo', async (req, res) => {
    try {
       const status = req.params.status
       const sleep = req.params.tempo
        const response = await axios.get(`http://httpstat.us/${status}?sleep=${sleep}`)
        const result = response.data
         if(array.includes(status)){
            res.status({ code: `${status}`, message: "Esse valor é um status válido. Disponivel no status HTTP " })
         }
         else if (status !=array){
            throw res.status(404).json({ erro: 'Esse valor não é um status valido ' })
         }
         else {
            res.json({ code: result.code, description: result.description })
         }
        
    } catch (err) {
        res.status(404).json({ erro: 'Esse valor não é um status valido ' })
    }
})


app.listen(port)
